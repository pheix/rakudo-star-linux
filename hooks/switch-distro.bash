#!/bin/bash

DISTRO=$1
DIRPREFIX="rakudo-star-"
DISTRODIR="./${DIRPREFIX}${DISTRO}"
CURDISTRODIR=`find . -mindepth 1 -maxdepth 1 -type d`
CURDISTFNAME=`basename ${CURDISTRODIR}`
CURDIST=`echo ${CURDISTFNAME} | sed "s/$DIRPREFIX//"`
DISTCONF="${CURDISTRODIR}/etc/fetch_core.txt"

if [ -z "${DISTRO}" ]; then
	echo Distro argument is missed
	exit 1;
fi

WORKDIR=`pwd`

CURMOARDIR=`find ${CURDISTRODIR}/src -mindepth 1 -maxdepth 1 -name 'moarvm*' -type d`
CURNQPDIR=`find ${CURDISTRODIR}/src -mindepth 1 -maxdepth 1 -name 'nqp*' -type d`
CURRAKUDODIR=`find ${CURDISTRODIR}/src -mindepth 1 -maxdepth 1 -name 'rakudo-[0-9]*' -type d`

TARGETMOAR=`find ${CURMOARDIR} -mindepth 1 -maxdepth 1 -type d`
TARGETNQP=`find ${CURNQPDIR} -mindepth 1 -maxdepth 1 -type d`
TARGETRAKUDO=`find ${CURRAKUDODIR} -mindepth 1 -maxdepth 1 -name 'rakudo-[0-9]*' -type d`

echo ${CURDIST}
echo ${DISTRODIR}
echo ${CURDISTRODIR}
echo ${CURMOARDIR}
echo ${CURNQPDIR}
echo ${CURRAKUDODIR}
echo ${TARGETMOAR}
echo ${TARGETNQP}
echo ${TARGETRAKUDO}

# clean
rm -rf ${CURDISTRODIR}/bin/raku-debug
rm -rf ${CURDISTRODIR}/bin/raku
cd ${CURDISTRODIR} && bin/rstar clean && cd ${WORKDIR}

# replace curr distro version in config
sed -i "s/$CURDIST/$DISTRO/" ${DISTCONF}

# checkout new version at local repos
echo -e "[**DEBUG] git checkout in ${TARGETMOAR}:"
cd ${TARGETMOAR} && git reset --hard HEAD
make clean
make distclean
git checkout $DISTRO
cat build/setup.pm | sed 's/-Werror=vla//' > build/_setup.pm && mv build/_setup.pm build/setup.pm # https://github.com/MoarVM/MoarVM/commit/05cc853ee1c6265543e6d3e9aabfe3b7c1dda0a4
cd ${WORKDIR}

echo -e "[**DEBUG] git checkout in ${TARGETNQP}:"
cd ${TARGETNQP} && git reset --hard HEAD
make clean
make distclean
git checkout $DISTRO && cd ${WORKDIR}

echo -e "[**DEBUG] git checkout in ${TARGETRAKUDO}:"
cd ${TARGETRAKUDO} && git reset --hard HEAD
make clean
make distclean
git checkout $DISTRO && cd ${WORKDIR}

# rename targets
for COMPONENT in MoarVM nqp rakudo
do
    SRC_CURRENT=
    DST_CURRENT=
    SRC_TARGET=
    DST_TARGET=

    if [ "${COMPONENT}" == "MoarVM" ]; then
        SRC_CURRENT=${CURMOARDIR}
        SRC_TARGET=${TARGETMOAR}
    elif [ "${COMPONENT}" == "nqp" ]; then
        SRC_CURRENT=${CURNQPDIR}
        SRC_TARGET=${TARGETNQP}
    else
        SRC_CURRENT=${CURRAKUDODIR}
        SRC_TARGET=${TARGETRAKUDO}
    fi

    DST_CURRENT=${CURDISTRODIR}/src/${COMPONENT,,}-${DISTRO}
    DST_TARGET=${SRC_CURRENT}/${COMPONENT}-${DISTRO}

	echo -e "[**DEBUG] processing ${COMPONENT}:"
	echo -e "\tSRC_CURRENT=${SRC_CURRENT}\n\tSRC_TARGET=${SRC_TARGET}\n\tDST_CURRENT=${DST_CURRENT}\n\tDST_TARGET=${DST_TARGET}"

    if ([ -d "${SRC_TARGET}" ] && [ -d "${DST_TARGET}" ] && [ "${SRC_TARGET}" != "${DST_TARGET}" ]); then
        echo "copy targets ${SRC_TARGET} ${DST_TARGET}"
        mv ${SRC_TARGET} ${DST_TARGET}
    fi

    if ([ -d "${SRC_CURRENT}" ] && [ -d "${DST_CURRENT}" ] && [ "${SRC_CURRENT}" != "${DST_CURRENT}" ]); then
        echo "copy currents ${SRC_CURRENT} ${DST_CURRENT}"
        mv ${SRC_CURRENT} ${DST_CURRENT}
    fi
done

echo "[**DEBUG]: current directory `pwd` (WORKDIR=${WORKDIR})"

# rename root
if ([ -d "${CURDISTRODIR}" ] && [ -d "${DISTRODIR}" ] && [ "${CURDISTRODIR}" != "${DISTRODIR}" ]); then
    mv ${CURDISTRODIR} ${DISTRODIR}
fi

# tar the dist - skip cause it's done at helpers/dojob.bash
# tar --exclude-vcs -hczf /resources/rakudo-star-${DISTRO}.tar.gz ${DISTRODIR}

echo All done
