#!/bin/bash

echo "hey, let's do the job..."

DISTR=$1
DEPTH=$2
RSGIT=$HOME/git/rakudo-star-linux
BUILD=$HOME/rakudo-star-build

if [ -z "${DEPTH}" ]; then
    DEPTH=10
fi

if [ ! -d $BUILD ]; then
    echo "build dir ${BUILD} is missed " && exit 1
fi

if [ -z "${DISTR}" ] || [ ! -f "${BUILD}/rakudo-star-${DISTR}.tar.gz" ]; then
    echo "wrong distributon version" && exit 2
fi

tar -xzf rakudo-star-${DISTR}.tar.gz
cp -rf ${RSGIT}/rstar/* ${BUILD}/rakudo-star-${DISTR}
cp ${RSGIT}/hooks/switch-distro.bash ${BUILD}

# fetch and configure the latest MoarVM
cd $HOME/rakudo-star-build/rakudo-star-${DISTR}/src/moarvm-${DISTR} && rm -rf MoarVM-${DISTR}/
git clone --depth=${DEPTH} -b main https://github.com/MoarVM/MoarVM.git MoarVM-${DISTR}
cd MoarVM-${DISTR} && perl Configure.pl

# fetch and configure the latest NQP
cd $HOME/rakudo-star-build/rakudo-star-${DISTR}/src/nqp-${DISTR} && rm -rf nqp-${DISTR}
git clone --depth=${DEPTH} -b main https://github.com/Raku/nqp.git nqp-${DISTR}
cd nqp-${DISTR}/3rdparty && git clone https://github.com/Raku/nqp-configure.git

# fetch and configure the latest rakudo
cd $HOME/rakudo-star-build/rakudo-star-${DISTR}/src/rakudo-${DISTR} && rm -rf rakudo-${DISTR}/
git clone --depth=${DEPTH} -b main https://github.com/rakudo/rakudo.git rakudo-${DISTR}
cd rakudo-${DISTR}/3rdparty && git clone https://github.com/Raku/nqp-configure.git

# install some missed modules
cd $HOME/rakudo-star-build/rakudo-star-${DISTR}/src/rakudo-star-modules
rm -rf zef \
       rakudoc \
       Digest \
       Digest-MD5 \
       Temp-Path \
       Hash-Merge \
       Log-Colored \
       IO-Path-XDG \
       Config-TOML \
       Config \
       Config-Parser-toml \
       Config-Parser-yaml \
       Term-termios \
       Terminal-ANSIParser \
       Terminal-LineEditor \
       DBIish \
       OpenSSL \
       PSGI \
       HTTP-Easy \
       Path-Finder \
       Pod-Usage \
       sigpipe \
       JSON-Tiny \
       RakudoPrereq \
       Debugger-UI-CommandLine \
       Log \
       HTTP-UserAgent
git clone https://github.com/ugexe/zef.git
git clone https://github.com/raku-community-modules/IO-MiddleMan.git
git clone https://github.com/JJ/raku-pod-utils.git Pod-Utils
git clone https://github.com/Raku/rakudoc.git
#git clone https://git.tyil.nl/raku/log Log
#git clone https://git.tyil.nl/raku/hash-merge Hash-Merge
#git clone https://git.tyil.nl/raku/log-colored Log-Colored
git clone https://git.sr.ht/~tyil/raku-hash-merge Hash-Merge
git clone https://git.sr.ht/~tyil/raku-log-colored Log-Colored
#git clone https://git.tyil.nl/raku/io-path-xdg IO-Path-XDG
git clone https://github.com/ugexe/Raku-PathTools.git
git clone https://github.com/atweiden/config-toml.git Config-TOML
git clone https://git.sr.ht/~tyil/raku-config Config
#git clone https://git.tyil.nl/raku/config Config
#git clone https://git.tyil.nl/raku/config-parser-toml Config-Parser-toml
#git clone https://git.tyil.nl/raku/config-parser-yaml Config-Parser-yaml
git clone https://github.com/japhb/Text-MiscUtils.git
git clone https://github.com/krunen/term-termios.git Term-termios
git clone https://github.com/japhb/Terminal-ANSIParser.git
git clone https://github.com/japhb/Terminal-Capabilities.git
git clone https://github.com/japhb/Terminal-LineEditor.git
git clone https://github.com/raku-community-modules/DBIish.git
git clone https://github.com/pheix/openssl.git OpenSSL
git clone https://github.com/raku-community-modules/PSGI.git
git clone https://github.com/raku-community-modules/HTTP-Easy.git
git clone https://github.com/zostay/HTTP-Request-FormData.git
git clone https://github.com/pheix/raku-trove.git Trove
git clone https://github.com/Leont/path-finder.git Path-Finder
git clone https://github.com/Leont/pod-usage.git Pod-Usage
git clone https://github.com/Leont/sigpipe.git
git clone https://github.com/grondilu/libdigest-raku.git Digest
git clone https://gitlab.com/jjatria/http-tiny.git HTTP-Tiny
git clone https://github.com/tony-o/perl6-data-dump.git Data-Dump
git clone https://github.com/melezhik/Sparrow6.git Sparrow6
git clone https://github.com/melezhik/sparky-job-api.git Sparky-JobApi
git clone https://github.com/moritz/json.git JSON-Tiny
git clone https://github.com/raku-community-modules/RakudoPrereq.git RakudoPrereq
git clone https://github.com/jnthn/rakudo-debugger.git Debugger-UI-CommandLine
git clone https://gitlab.com/pheix/http-useragent.git HTTP-UserAgent
git clone https://github.com/masak/xml-writer.git XML-Writer
cd $HOME/rakudo-star-build/rakudo-star-${DISTR}/src/rakudo-star-modules/Hash-Merge && git checkout 95e89dee79200e1ea75b1ef976a30bf6b61e23a7

# patch original Rakudo Star bundle
cd $HOME/rakudo-star-build && bash switch-distro.bash ${DISTR}

# pack the rakudo star bundle
cd $HOME/rakudo-star-build && tar --exclude-vcs -hczf /resources/rakudo-star-${DISTR}.tar.gz ./rakudo-star-${DISTR}

echo "job's done!"
