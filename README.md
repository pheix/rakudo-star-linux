
# Rakudo Star bundle for Linux

Tested and ready for deployment Rakudo Star bundle for Linux platform. Please check official [Wiki pages](https://github.com/rakudo/star/wiki/01_Rakudo-Star---Linux-package).

## Revision

[2025.02](https://rakudo.org/post/announce-rakudo-release-2025.02)

## Quick start

```bash
sudo mkdir /resources
cd $HOME/git && git clone https://gitlab.com/pheix/rakudo-star-linux.git
mkdir $HOME/rakudo-star-build && cd $_
wget https://github.com/rakudo/star/releases/download/2025.02/rakudo-star-2025.02.tar.gz
bash $HOME/git/rakudo-star-linux/helpers/dojob.bash 2025.02
```

## Manual build & deploy

Get and copy patches to original Rakudo Star bundle:

```bash
mkdir $HOME/rakudo-build && cd $_
wget https://github.com/rakudo/star/releases/download/2025.02/rakudo-star-2025.02.tar.gz
tar -xzf rakudo-star-2025.02.tar.gz
cp -rf $HOME/git/rakudo-star-linux/rstar/* rakudo-star-2025.02
cp $HOME/git/rakudo-star-linux/hooks/switch-distro.bash .
```

Get latest `MoarVM`, `nqp` and `Rakudo` versions:

```bash
cd $HOME/rakudo-star-build/rakudo-star-2025.02/src/moarvm-2025.02 && rm -rf MoarVM-2025.02/
git clone https://github.com/MoarVM/MoarVM.git MoarVM-2025.02
cd MoarVM-2025.02 && perl Configure.pl
cd $HOME/rakudo-star-build/rakudo-star-2025.02/src/nqp-2025.02 && rm -rf nqp-2025.02
git clone https://github.com/Raku/nqp.git nqp-2025.02
cd nqp-2025.02/3rdparty && git clone https://github.com/Raku/nqp-configure.git
cd $HOME/rakudo-star-build/rakudo-star-2025.02/src/rakudo-2025.02 && rm -rf rakudo-2025.02/
git clone https://github.com/rakudo/rakudo.git rakudo-2025.02
cd rakudo-2025.02/3rdparty && git clone https://github.com/Raku/nqp-configure.git
```

Install some missed modules:

```bash
cd $HOME/rakudo-star-build/rakudo-star-2025.02/src/rakudo-star-modules
rm -rf zef \
       rakudoc \
       Digest \
       Digest-MD5 \
       Temp-Path \
       Hash-Merge \
       Log-Colored \
       IO-Path-XDG \
       Config-TOML \
       Config \
       Config-Parser-toml \
       Config-Parser-yaml \
       Term-termios \
       Terminal-ANSIParser \
       Terminal-LineEditor \
       DBIish \
       OpenSSL \
       PSGI \
       HTTP-Easy \
       Path-Finder \
       Pod-Usage \
       sigpipe \
       JSON-Tiny \
       RakudoPrereq \
       Debugger-UI-CommandLine \
       Log \
       HTTP-UserAgent
git clone https://github.com/ugexe/zef.git
git clone https://github.com/raku-community-modules/IO-MiddleMan.git
git clone https://github.com/JJ/raku-pod-utils.git Pod-Utils
git clone https://github.com/Raku/rakudoc.git
#git clone https://git.tyil.nl/raku/log Log
#git clone https://git.tyil.nl/raku/hash-merge Hash-Merge
#git clone https://git.tyil.nl/raku/log-colored Log-Colored
git clone https://git.sr.ht/~tyil/raku-hash-merge Hash-Merge
git clone https://git.sr.ht/~tyil/raku-log-colored Log-Colored
#git clone https://git.tyil.nl/raku/io-path-xdg IO-Path-XDG
git clone https://github.com/ugexe/Raku-PathTools.git
git clone https://github.com/atweiden/config-toml.git Config-TOML
git clone https://git.sr.ht/~tyil/raku-config
#git clone https://git.tyil.nl/raku/config Config
#git clone https://git.tyil.nl/raku/config-parser-toml Config-Parser-toml
#git clone https://git.tyil.nl/raku/config-parser-yaml Config-Parser-yaml
git clone https://github.com/japhb/Text-MiscUtils.git
git clone https://github.com/krunen/term-termios.git Term-termios
git clone https://github.com/japhb/Terminal-ANSIParser.git
git clone https://github.com/japhb/Terminal-Capabilities.git
git clone https://github.com/japhb/Terminal-LineEditor.git
git clone https://github.com/raku-community-modules/DBIish.git
git clone https://github.com/pheix/openssl.git OpenSSL
git clone https://github.com/raku-community-modules/PSGI.git
git clone https://github.com/raku-community-modules/HTTP-Easy.git
git clone https://github.com/zostay/HTTP-Request-FormData.git
git clone https://github.com/pheix/raku-trove.git Trove
git clone https://github.com/Leont/path-finder.git Path-Finder
git clone https://github.com/Leont/pod-usage.git Pod-Usage
git clone https://github.com/Leont/sigpipe.git
git clone https://github.com/grondilu/libdigest-raku.git Digest
git clone https://gitlab.com/jjatria/http-tiny.git HTTP-Tiny
git clone https://github.com/tony-o/perl6-data-dump.git Data-Dump
git clone https://github.com/melezhik/Sparrow6.git Sparrow6
git clone https://github.com/melezhik/sparky-job-api.git Sparky-JobApi
git clone https://github.com/moritz/json.git JSON-Tiny
git clone https://github.com/raku-community-modules/RakudoPrereq.git RakudoPrereq
git clone https://github.com/jnthn/rakudo-debugger.git Debugger-UI-CommandLine
git clone https://gitlab.com/pheix/http-useragent.git HTTP-UserAgent
git clone https://github.com/masak/xml-writer.git XML-Writer
cd $HOME/rakudo-star-build/rakudo-star-2025.02/src/rakudo-star-modules/Hash-Merge && git checkout 95e89dee79200e1ea75b1ef976a30bf6b61e23a7
```

Patch original Rakudo Star bundle:

```bash
cd $HOME/rakudo-star-build && bash switch-distro.bash 2025.02
```

This is deprecated step, it was used by own `install_raku_module()` [implementation](https://gitlab.com/pheix/rakudo-star-linux/-/blob/main/rstar/lib/actions/install.bash?ref_type=heads#L251). Now we use `zef` to install modules, so any manual patching for `OpenSSL` is not needed. I keep this just as memo.

> Add current work directory path `$cwd` to `resources/libraries.json` at OpenSSL `Build` [unit class](https://github.com/sergot/openssl/blob/main/Build.rakumod#L27):
>
>```perl
>"$cwd/resources/libraries.json".IO.spurt: $json;
>```

Pack your own Rakudo Star bundle build:

```bash
cd $HOME/rakudo-star-build && tar --exclude-vcs -hczf /resources/rakudo-star-2025.02.tar.gz ./rakudo-star-2025.02
```

Build and deploy:

```bash
cd /resources && tar -xzf rakudo-star-2025.02.tar.gz
cd /resources/rakudo-star-2025.02 && bin/rstar install
```

Clean (optional):

```bash
cd /resources/rakudo-star-2025.02 && bin/rstar clean
cd / && rm -rf /resources/rakudo-star-2025.02
```

## Rakudo Star repository

https://github.com/rakudo/star

## Author

Please contact me via [Matrix](https://matrix.to/#/@k.narkhov:matrix.org) or [LinkedIn](https://www.linkedin.com/in/knarkhov/). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
